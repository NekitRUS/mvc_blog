<?php
class Model_blogs extends Model{
    public function get_data($blog) {	
        return CommentsDB::getInstance()->Select($blog);
    }
    
    public function insert_data($obj){
        CommentsDB::getInstance()->InsertComment($obj);
    }
}

class CommentsDB extends mysqli{
    
    private static $instance = null;
    
    private function __construct() {
        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        $this->query("CREATE DATABASE IF NOT EXISTS " . DB_NAME .
            " DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
        $this->query("USE " . DB_NAME);
        
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
        $this->query("CREATE TABLE IF NOT EXISTS comments(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            blog_id INT NOT NULL,
            user_login VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            date DATETIME NOT NULL,
            text TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            FOREIGN KEY (user_login) REFERENCES users(login),
            FOREIGN KEY (blog_id) REFERENCES blogs(id)
            )
            DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function DateTimeForSQL($date) {
        return $date->format("Y-m-d H:i:s");
    }
    
    public function InsertComment($obj) {
        $this->query("INSERT INTO comments (blog_id, user_login, text, date)" .
                 " VALUES ('" . (int)$obj['blog_id'] . "', '" . $this->real_escape_string($obj['user_login']) . "', '" . 
                 $this->real_escape_string($obj['text']). "', '" . $this->DateTimeForSQL(new DateTime()) . "')");
    }
    
    public function Select($blog){
        $array = array();
        
        $blogResult = $this->query("SELECT blogs.id AS id, user_login, date, caption, content, e_mail, users.id AS user_id
                 FROM blogs JOIN users
                 ON blogs.user_login = users.login
                 WHERE blogs.id='" . (int)$blog . "'");
        if($blogResult->num_rows > 0){
            $temp = $blogResult->fetch_array();
            $array['blog'] = ['id' => (int)$temp['id'], 'user_login' => htmlspecialchars($temp['user_login'], ENT_HTML5, ""), 'caption' => htmlspecialchars($temp['caption'], ENT_HTML5, ""), 'content' => htmlspecialchars($temp['content'], ENT_HTML5, ""), 'date' => new DateTime($temp['date']), 'e_mail' => htmlspecialchars($temp['e_mail'], ENT_HTML5, ""), 'user_id' => (int)$temp['user_id']];
        }
        else {
            return false;
        }
        
        $array['comments'] = array();
        $commentsResult = $this->query("SELECT comments.user_login, comments.date, comments.text, users.e_mail, users.id
            FROM comments
            JOIN users
            ON comments.user_login = users.login
            WHERE blog_id='" . (int)$blog . "'"
            . " ORDER BY comments.date");
        if($commentsResult){
            while($row = $commentsResult->fetch_array()){
                $array['comments'][] = ['user_login' => htmlspecialchars($row['user_login'], ENT_HTML5, ""), 'e_mail' => htmlspecialchars($row['e_mail'], ENT_HTML5, ""), 'text' => htmlspecialchars($row['text'], ENT_HTML5, ""), 'date' => new DateTime($row['date']), 'user_id' => (int)$row['id']];
            }
        }
        
        return $array;
    }
}