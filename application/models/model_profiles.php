<?php
class Model_profiles extends Model{
    
    public function get_user($user){	
        return ProfilesDB::getInstance()->GetUser($user);
    }
    
    public function get_data(){	
        return ProfilesDB::getInstance()->Select();
    }
}

class ProfilesDB extends mysqli{
    
    private static $instance = null;
    
    private function __construct() {
        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        
        $this->query("CREATE DATABASE IF NOT EXISTS " . DB_NAME .
            " DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
        $this->query("USE " . DB_NAME);
        
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    public function GetUser($userID){
        $array = array();
        $result = $this->query("SELECT * FROM users WHERE id='" . (int)$userID . "'");
        if($result){
            if($result->num_rows > 0){
                $temp = $result->fetch_array();
                return $array['profile'] =  ['login' => htmlspecialchars($temp['login'], ENT_HTML5, ""),
                                            'full_name' => htmlspecialchars($temp['full_name'], ENT_HTML5, ""),
                                            'e_mail' => htmlspecialchars($temp['e_mail'], ENT_HTML5, ""),
                                            'vk' => htmlspecialchars($temp['vk'], ENT_HTML5, ""),
                                            'linkedIn' => htmlspecialchars($temp['linkedIn'], ENT_HTML5, ""),
                                            'twitter' => htmlspecialchars($temp['twitter'], ENT_HTML5, ""),
                                            'gplus' => htmlspecialchars($temp['gplus'], ENT_HTML5, ""),
                                            'avatar' => htmlspecialchars($temp['avatar'], ENT_HTML5, ""),
                                            'details' => htmlspecialchars($temp['details'], ENT_HTML5, "")];
            }
        }         
        return false; 
    }
    
    public function Select(){
        $array = array();
        $result = $this->query("SELECT id, login, avatar
                 FROM users
                 ORDER BY login");
        if($result){
            while($row = $result->fetch_array()){
                $array[] = ['id' => (int)$row['id'], 'login' => htmlspecialchars($row['login'], ENT_HTML5, ""), 'avatar' => htmlspecialchars($row['avatar'], ENT_HTML5, "")];
            }
        }
        return $array;
    }
}