<?php
class Model_auth extends Model{
    
    public function __construct() {
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/files/avatars")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/files/avatars", 0777, true);
        }
    }

    public function get_user($obj){	
        return UsersDB::getInstance()->GetUser($obj);
    }
    
    public function get_verify($obj){	
        return UsersDB::getInstance()->Exist($obj);
    }
    
    public function insert_data($obj){	
        return UsersDB::getInstance()->AddUser($obj);
    }
}

class UsersDB extends mysqli{
    
    private static $instance = null;
    
    private function __construct() {
        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        
        $this->query("CREATE DATABASE IF NOT EXISTS " . DB_NAME .
            " DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
        $this->query("USE " . DB_NAME);
        
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
        $this->query("CREATE TABLE IF NOT EXISTS users(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            login VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci UNIQUE NOT NULL,
            password VARCHAR(32) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            salt VARCHAR(20) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            e_mail VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            full_name VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            vk VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci,
            linkedIn VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci,
            twitter VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci,
            gplus VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci,
            avatar VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci,
            details TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci
            )
            DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function GetRandomSalt() {
        $characters = '!@#$%^&*()_+�;:?-=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomSalt = '';
        for ($i = 0; $i < rand(9, 19); $i++) {
            $randomSalt .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomSalt;
    }
     
    private function Encrypt($rawPassword, $salt){
        return md5(strrev(md5($salt . $rawPassword)));
    }
    
    private function RightAddress($str) {
        if(substr($str, 0, 7) == 'http://') {
            $str = str_replace("http://", "", $str);
        }
        if(substr($str, 0, 8) == 'https://'){
            $str = str_replace("https://", "", $str);
        }
        return 'https://' . $str;
    }
    
    public function Exist($data){
        $login = $this->real_escape_string($data['login']);
        $e_mail = $this->real_escape_string($data['e_mail']);
        $result = $this->query("SELECT * FROM users WHERE login='" . $login . "'");
        if($result){
            if($result->num_rows > 0){
                return true;
            }
        }    
        $result = $this->query("SELECT * FROM users WHERE e_mail='" . $e_mail . "'");
        if($result){
            if($result->num_rows > 0){
                return true;
            }
        }  
        return false; 
    }

    public function AddUser($data){
        $full_name = $this->real_escape_string($data['name']);
        $login = $this->real_escape_string($data['login']);
        $e_mail = $this->real_escape_string($data['e_mail']);
        $vk = !empty($data['vk']) ? $this->RightAddress($this->real_escape_string($data['vk'])) : null;
        $linkedIn = !empty($data['linkedIn']) ? $this->RightAddress($this->real_escape_string($data['linkedIn'])) : null;
        $twitter = !empty($data['twitter']) ? $this->RightAddress($this->real_escape_string($data['twitter'])) : null;
        $gplus = !empty($data['gplus']) ? $this->RightAddress($this->real_escape_string($data['gplus'])) : null;
        $avatar = !empty($data['avatar']['name']) ? $data['avatar'] : null;
        if (!empty($avatar['name'])) {
            $id = $this->query("SHOW TABLE STATUS WHERE name='users'");
            $id = $id->fetch_array();
            $id = $id['Auto_increment'];
            $fileName = "/files/avatars/" . $id . substr($avatar['name'], strrpos($avatar['name'], "."));
            if (move_uploaded_file($avatar['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $fileName)) {
                $avatar = $fileName;
            }
            else {
                $avatar = null;
            }
        }
        $details = !empty($data['details']) ? $this->real_escape_string($data['details']) : null;
        $salt = $this->GetRandomSalt();
        $password = $this->Encrypt($data['password'], $salt);
        $result = $this->query("SELECT * FROM users WHERE login='" . $login . "'");
        if($result){
            if (mysqli_num_rows($result) > 0){ return false; }
        }
        return $this->query("INSERT INTO users (full_name, login, e_mail, password, salt, vk, linkedIn, twitter, gplus, avatar, details)"
                . "VALUES ('" . $full_name . "', '"
                . $login . "', '"
                . $e_mail . "', '"
                . $password . "', '"
                . $salt . "', '"
                . $vk . "', '"
                . $linkedIn . "', '"
                . $twitter . "', '"
                . $gplus . "', '"
                . $avatar . "', '"
                . $details . "')");
    }
    
    public function GetUser($data){
        $login = $this->real_escape_string($data['login']);
        $rawPassword = $this->real_escape_string($data['password']);
        $result = $this->query("SELECT login, password, salt, e_mail, full_name FROM users WHERE login='" . $login . "'");
        if($result){
            $user = $result->fetch_array();
            if(($user['login'] == $login) && ($user['password'] == $this->Encrypt($rawPassword, $user['salt']))){
                return ['login' => htmlspecialchars($user['login'], ENT_HTML5, ""), 'full_name' => htmlspecialchars($user['full_name'], ENT_HTML5, ""), 'e_mail' => htmlspecialchars($user['e_mail'], ENT_HTML5, "")];
            }
        }         
        return false;
    }
}