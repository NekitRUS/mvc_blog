<?php
class Model_main extends Model{
    public function get_data() {	
        return BlogsDB::getInstance()->Select();
    }
    
    public function insert_data($obj){
        BlogsDB::getInstance()->InsertBlog($obj);
    }
}

class BlogsDB extends mysqli{
    
    private static $instance = null;
    
    private function __construct() {
        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        $this->query("CREATE DATABASE IF NOT EXISTS " . DB_NAME .
            " DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
        $this->query("USE " . DB_NAME);
        
        $this->query("SET character_set_client='cp1251'");
        $this->query("SET character_set_connection='cp1251'");
        $this->query("SET character_set_results='cp1251'");
        $this->query("CREATE TABLE IF NOT EXISTS blogs(
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            user_login VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            date DATETIME NOT NULL,
            caption VARCHAR(200) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            content LONGTEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL,
            FOREIGN KEY (user_login) REFERENCES users(login)
            )
            DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;");
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function DateTimeForSQL($date) {
        return $date->format("Y-m-d H:i:s");
    }
    
    public function InsertBlog($obj) {
        $this->query("INSERT INTO blogs (user_login, caption, content, date)" .
                 " VALUES ('" . $this->real_escape_string($obj['Name']) . "', '" . $this->real_escape_string($obj['Caption']) . "', '" . 
                 $this->real_escape_string($obj['Content']) . "', '" . $this->DateTimeForSQL(new DateTime()) . "')");
    }
    
    public function Select(){
        $array = array();
        $result = $this->query("SELECT blogs.id AS id, user_login, date, caption, LEFT(content, 1000), e_mail, users.id AS user_id
                 FROM blogs JOIN users
                 ON blogs.user_login = users.login
                 ORDER BY blogs.id DESC");
        if($result){
            while($row = $result->fetch_array()){
                $array[] = ['Name' => htmlspecialchars($row['user_login'], ENT_HTML5, ""), 'Email' => htmlspecialchars($row['e_mail'], ENT_HTML5, ""), 'Caption' => htmlspecialchars($row['caption'], ENT_HTML5, ""), 'Content' => htmlspecialchars($row['LEFT(content, 1000)'], ENT_HTML5, ""), 'Date' => new DateTime($row['date']), 'Number' => (int)$row['id'], 'user_id' => (int)$row['user_id']];
            }
        }
        return $array;
    }
}