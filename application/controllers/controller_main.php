<?php
Class Controller_main extends Controller{
    function __construct() {
        $this->model = new Model_main();
        $this->view = new View();
        session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index() {
        $data = $this->model->get_data();
        $this->view->generate('main_view.php', 'template_view.php', $data);
    }
    
    function action_add() {
        $captionIsEmpty = false;
        $textIsEmpty = false;
        $textTooLong = false;
        $error = array();
        $usersData = array();
        $post = $_SERVER["REQUEST_METHOD"] == "POST";
        $logged = isset($_SESSION['login']);
            
        if ($post) {
            $usersData = ['caption' => trim($_POST['caption']), 'text' => trim($_POST['text'])];

            if ($logged) {
                $usersData['user'] = $_SESSION['login'];
            }
            else {
                $error['logged'] = true;
            }
            
            if ($usersData['caption'] == "") {
                $captionIsEmpty = true;
                $error['caption'] = true;
            }
            if ($usersData['text'] == "") {
                $textIsEmpty = true;
                $error['textEmpty'] = true;
            }
            if (strlen($usersData["text"]) > 15000000) {
                $textTooLong = true;
                $error['textLong'] = true;
            }

            if ($logged && !$captionIsEmpty && !$textIsEmpty && !$textTooLong) {
                $this->model->insert_data(['Name' => $usersData["user"], 'Caption' => $usersData["caption"], 'Content' => $usersData["text"]]);
                $usersData = array();
                header("Location: /main");
                exit();
            }
        }
        
        $data['error'] = $error;
        $data['info'] = $usersData;
        $this->view->generate('add_view.php', 'template_view.php', $data);
    }
}