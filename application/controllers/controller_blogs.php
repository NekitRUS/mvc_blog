<?php
class Controller_blogs extends Controller{
    function __construct() {
        $this->model = new Model_blogs();
        $this->view = new View();
        session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index() {
        header("Location: /main");
    }
    
    function action_blog($blogID) {
        
        $textIsEmpty = false;
        $textTooLong = false;
        $error = array();
        $usersData = array();
        $post = $_SERVER["REQUEST_METHOD"] == "POST";
        $logged = isset($_SESSION['login']);
            
        if ($post) {
            $usersData['text'] = trim($_POST['text']); 

            if ($logged) {
                $usersData['user'] = $_SESSION['login'];
            }
            else {
                $error['logged'] = true;
            }
            
            if ($usersData['text'] == "") {
                $textIsEmpty = true;
                $error['textEmpty'] = true;
            }
            if (strlen($usersData["text"]) > 65000) {
                $textTooLong = true;
                $error['textLong'] = true;
            }

            if ($logged && !$textIsEmpty && !$textTooLong) {
                $this->model->insert_data(['blog_id' => $blogID, 'user_login' => $usersData["user"], 'text' => $usersData["text"]]);
                $usersData = array();
                header("Refresh:0");
                exit();
            }
        }
        
        $blog = $this->model->get_data($blogID);
        if ($blog) {
            $data['info'] = $blog;
        }
        else {
            header("Location: /404");
            exit();
        }
        $data['error'] = $error;
        $data['input'] = $usersData;
        $this->view->generate('blog_view.php', 'template_view.php', $data);
    }
}