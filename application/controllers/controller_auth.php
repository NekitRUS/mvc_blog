<?php
Class Controller_auth extends Controller{
    function __construct() {
        $this->model = new Model_auth();
        $this->view = new View();
    }
    
    function action_index() {
        $logLoginIsEmpty = false;					
        $logPasswordIsEmpty = false;
        $logUser = false;
        $regLoginIsEmpty = false;
        $regPasswordIsEmpty = false;
        $regNameIsEmpty = false;
        $regEmailIsEmpty = false;
        $regUser = false;
        $fileError = false;
        $error = array();
        $usersData = array();
        $post = $_SERVER["REQUEST_METHOD"] == "POST";
        $login = isset($_POST['log']);
        $registration = isset($_POST['reg']);
        
        session_id(1); //Local virtual host fix...
        session_start();
        session_unset(); 
        session_destroy();

        if ($post && $login) {
            $usersData = ['login' => trim($_POST['logLogin']), 'password' => trim($_POST['logPassword'])];

            if ($usersData["login"] == "") {
                $logLoginIsEmpty = true;
            }
            if ($usersData["password"] == "") {
                $logPasswordIsEmpty = true;
            }

            if (!$logLoginIsEmpty && !$logPasswordIsEmpty) {
                $logUser = $this->model->get_user($usersData);
                if($logUser){
                    session_id(1); //Local virtual host fix...
                    session_start();
                    $nameParts = explode(' ', $logUser['full_name']);
                    $greeting = "";
                    if(!empty($nameParts[1])){
                        $greeting .= $nameParts[1];
                    }
                    if(!empty($nameParts[2])){
                        $greeting .= " " . $nameParts[2];
                    }
                    $_SESSION['full_name'] = $greeting;
                    $_SESSION['login'] = $logUser['login'];
                    session_write_close();
                    header('location: /main');
                    exit;
                }
                else{
                    $error['mismatch'] = true;
                }
            }
        }
        
        if ($post && $registration) {
            $usersData = ['regLogin' => trim($_POST['regLogin']),
                'regPassword' => trim($_POST['regPassword']),
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'vk' => trim($_POST['vk']),
                'linkedIn' => trim($_POST['linkedIn']),
                'twitter' => trim($_POST['twitter']),
                'gplus' => trim($_POST['gplus']),
                'avatar' => trim( $_FILES['avatar_fake']['name']),
                'details' => trim($_POST['details']) ];
            
            if ($usersData["regLogin"] == "") {
                $regLoginIsEmpty = true;
                $error['login'] = true;
            }
            if ($usersData["regPassword"] == "") {
                $regPasswordIsEmpty = true;
                $error['password'] = true;
            }
            if ($usersData["name"] == "") {
                $regNameIsEmpty = true;
                $error['name'] = true;
            }
            if ($usersData["email"] == "") {
                $regEmailIsEmpty = true;
                $error['email'] = true;
            }
            if (!empty($usersData['avatar'])) {
                if (($_FILES['avatar_fake']['size'] / 1024) > 700) {
                    $fileError = true;
                    $error['file'] = true;
                }
                if ((strtolower(substr($usersData['avatar'], strlen($usersData['avatar']) - 4, 4)) != ".png") && (strtolower(substr($usersData['avatar'], strlen($usersData['avatar']) - 5, 5)) != ".jpeg") && (strtolower(substr($usersData['avatar'], strlen($usersData['avatar']) - 4, 4)) != ".jpg")) {
                    $fileError = true;
                    $error['file'] = true;
                }
            }
            if (!$regLoginIsEmpty && !$regPasswordIsEmpty && !$regNameIsEmpty && !$regEmailIsEmpty && !$fileError){
                $regUser = $this->model->get_verify(['login' => $usersData['regLogin'], 'e_mail' => $usersData['email']]);
                if($regUser){
                    $error['alreadyExists'] = true;
                }
                else{
                    $this->model->insert_data([ 'login' => $usersData['regLogin'],
                        'password' => $usersData['regPassword'],
                        'name' => $usersData['name'],
                        'e_mail' => $usersData['email'],
                        'vk' => $usersData['vk'],
                        'linkedIn' => $usersData['linkedIn'],
                        'twitter' => $usersData['twitter'],
                        'gplus' => $usersData['gplus'],
                        'avatar' => $_FILES['avatar_fake'],
                        'details' => $usersData['details'] ]);
                    $this->send_email(['name' => $usersData['name'], 'address' => $usersData['email'], 'login' => $usersData['regLogin'], 'password' => $usersData['regPassword']]);
                    session_id(1); //Local virtual host fix...
                    session_start();
                    $nameParts = explode(' ', $usersData['name']);
                    $greeting = "";
                    if(!empty($nameParts[1])){
                        $greeting .= $nameParts[1];
                    }
                    if(!empty($nameParts[2])){
                        $greeting .= " " . $nameParts[2];
                    }
                    if (empty($greeting)) {
                        $greeting = $nameParts[0];
                    }
                    $_SESSION['full_name'] = $greeting;
                    $_SESSION['login'] = $usersData['regLogin'];
                    session_write_close();
                    header('Location: /main');
                    exit;
                }
            }
        }
        
        $data['error'] = $error;
        $data['info'] = $usersData;
        $this->view->generate('auth_view.php', 'template_view.php', $data);
    }
    
    function send_email ($data) {
        $name = $data['name'];
        $to = $data['address'];
        $login = $data['login'];
        $password = $data['password'];
        $message = "������������, $name!
����������� � ������������ �� www.bestpractices.ml
��� �����: $login
��� ������: $password
�������� �� ������ ��������� �� �����.
�� �������� ��� ��������� ������, ��� ��� e-mail ����� ��� ��������������� �� ����� www.bestpractices.ml.
���� �� �� ���������������� �� ���� �����, ����������, �������������� ��� ������.
___
� ���������� �����������, ������������� ����� www.bestpractices.ml.";
        
        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "From: =?windows-1251?B?" . base64_encode("Tiny Blogs co.") . "?= <noreply@bestpractices.ml>";
        $headers[] = "To: =?windows-1251?B?" . base64_encode($name) . "?= <$to>";
        $headers[] = "Subject: =?windows-1251?B?" . base64_encode("�����������") . "?=";
    
        return mail($to, "�����������", $message, implode("\r\n", $headers));
    }
}