<?php
Class Controller_profiles extends Controller{
    function __construct() {
        $this->model = new Model_profiles();
        $this->view = new View();
        session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index() {
        $data = $this->model->get_data();
        $this->view->generate('profiles_view.php', 'template_view.php', $data);
    }
    
    function action_profile($userID) {
        $result = $this->model->get_user($userID);
        if ($result) {
            $data['user'] = $result;
        }
        else {
            header("Location: /404");
            exit();
        }
        $this->view->generate('profile_view.php', 'template_view.php', $data);
    }
}