<?php
class Controller_404 extends Controller{
    function __construct() {
        $this->model = new Model();
        $this->view = new View();
        session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index() {
        $this->view->generate('404_view.php', 'template_view.php');
    }
}
