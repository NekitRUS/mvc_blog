<?php
class Route {
    static function Start(){
        $controller_name = 'main';
        $action_name = 'index';
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        
        if(!empty($routes[1])){
            $controller_name = $routes[1];
        }
        
        if(!empty($routes[2])){
            $action_name = $routes[2];
        }
        
        $query1 = (!empty($routes[3])) ? $routes[3] : null; 

        $query2 = (!empty($routes[4])) ? $routes[4] : null; 

        
        $model_name = 'Model_' . $controller_name;
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'Action_' . $action_name;
        
        $model_path = "application/models/" . strtolower($model_name) . '.php';
        if(file_exists($model_path)){
            include $model_path;
        }
        
        $controller_path = "application/controllers/" . strtolower($controller_name) . '.php';
        if(file_exists($controller_path)){
            include $controller_path;
        }
        else{
            Route::ErrorPage404();
        }     
        
        $controller = new $controller_name;
        if(method_exists($controller, $action_name)){
            $controller->$action_name($query1, $query2);
        }                
        else {
            Route::ErrorPage404();
        }
    }
    
    function ErrorPage404()
    {
        header('HTTP/1.1 404 Not Found');
        //header("Status: 404 Not Found");
        header('Location: /404');
    }
}