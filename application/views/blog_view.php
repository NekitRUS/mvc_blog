<section class="card" id="blog">
<?php
    $output = "";
    $number = $data['info']['blog']['id'];
    $output .="<H1>#" . $number . " " . nl2br($data['info']['blog']['caption'])  . "</H1>" 
            . "<div id='date'>" . date_format($data['info']['blog']['date'], 'j F Y G:i') . "</div>"
            .  "<div id='author'><strong>@</strong><a id='profile' href=/profiles/profile/" . $data['info']['blog']['user_id'] . ">" . $data['info']['blog']['user_login'] . "</a> [<a href=mailto:" . $data['info']['blog']['e_mail'] . ">E-mail</a>]</div>"
            . "<div id='content'>" . nl2br($data['info']['blog']['content']) . "</div>";
    echo $output;

    $commentsCount = count($data['info']['comments']);
    
    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    
    $title = "���� �� ����� Tiny Blogs";
    $title = iconv("Windows-1251","UTF-8", $title);
    $title = urlencode($title);
    
    $description = $data['info']['blog']['caption'];
    $description = iconv("Windows-1251","UTF-8", $description);
    $description = urlencode($description);
?>

    <div id="share">
        ����������: <a target="_blank"
            title="������������ ������ � Twitter"
            href="http://twitter.com/share?url=<?php echo $url; ?>&text=<?php echo (strlen($description) > 80 ? substr($description . "...", 0, 80) : $description); ?>&via=@tinyBlogs">
            <img src="/images/twitter32.png">
        </a>
        <a target="_blank"
            title="������������ ������ � LinkedIn"
            href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>">
            <img src="/images/linkedin32.png">
        </a>
        <a target="_blank"
            title="������������ ������ �� ���������"
            target="_blank"
            href="http://vk.com/share.php?url=<?php echo $url; ?>&title=<?php echo $title; ?>&description=<?php echo $description; ?>">
            <img src="/images/vk32.png">
        </a>
        <a target="_blank"
            title="������������ ������ � Google Plus"
            href="https://plus.google.com/share?url=<?php echo $url; ?>">
            <img src="/images/gplus32.png">
        </a>
    </div>
</section>
    
<section class="card" id="comment">
    <H1>����������� (<?php echo $commentsCount; ?>)</H1>
</section>

<?php
    $output = "";
    for ($i = $commentsCount - 1; $i >= 0 ; $i--) {
        $output .= "<section class='card' id='comment'>";
        $output .="<div id='author'>#" . ($i + 1)
                . " <strong>@</strong><a id='profile' href=/profiles/profile/" . $data['info']['comments'][$i]['user_id'] . ">" . $data['info']['comments'][$i]['user_login'] . "</a>"
                . " [<a href=mailto:" . $data['info']['comments'][$i]['e_mail'] . ">E-mail</a>]</div>"
                . "<div id='date'>" . date_format($data['info']['comments'][$i]['date'], 'j F Y') . " � " . date_format($data['info']['comments'][$i]['date'], 'G:i:s') . "</div>"
                . "<div id='content'>" . nl2br($data['info']['comments'][$i]['text']) . "</div>";
        $output .= "</section>";
    }
    echo $output;
?>

<section class="card" id="comment">
    <H3>�������� �����������</H3>

    <form id="post_comment" action=<?php echo ( "'/blogs/blog/" . $data['info']['blog']['id'] . "'") ; ?> method="POST">

        <div id="form"><textarea name="text" rows="6" maxlength="65000"><?php echo isset($data["input"]["text"]) ? $data["input"]["text"] : null; ?></textarea><br/></div>
        <?php if (!isset($_SESSION['login'])) { echo "<H2>������ ������������������ ������������ ����� ��������� �����������</H2>"; }
        if (isset($data["error"]["textEmpty"])) { echo ("<p>����������, ������� �����������!</p>"); }
        if (isset($data["error"]["textLong"])) { echo ("<p>����������� ������ ���� ����� 65 000 ��������!</p>"); }
        //if (isset($data["error"]["logged"])) { echo ("<p>��� ���������� ����������� ��� ���������� ����� �� ����!</p>"); }
        ?>

        <div id="form"><input type="submit" value="���������"/></div>
    </form>
</section>